﻿
#include <iostream>
#include <list>
#include <string>
#include <vector>
using namespace std;

struct Engine
{
	int I;
	vector<int> M;
	vector<int> V;
	int tOverheat;
	float Hm;
	float Hv;
	float C;
};

int main()
{
#pragma region Initialize
	Engine engine
	{
		10,
		vector<int> { 20, 75, 100, 105, 75, 0 },
		vector<int> { 0, 75, 150, 200, 250, 300 },
		110,
		0.01f,
		0.0001f,
		0.1f
	};

	//Ambient temperature
	int tAmbient = 0;

	//logic data
	int tick = 0;       //Time count
	float t;            //Start tempearture
	int stage = 0;      //Engine controller
	float V = 0;        //Crankshaft speed
	float a;            //Crankshaft acceleration

	float Vh;           //Speed of heating
	float Vc;           //Speed of freezing
#pragma endregion

	//Checking for correct data entry
	cout << "Enter ambient trmperature (int) \nATTENTION! \nPlease don't enter small and negative values, the program execution time will increase. Thank you.\n";
	while (!(cin >> tAmbient))
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "ERROR: Incorrect value \nEnter new value \n";
	}
	std::cout << tAmbient;



	t = tAmbient;
	a = engine.M[stage] / engine.I;
	bool maxStage = false;

	cout << "Engine on\n";

	while (true)
	{
		cout << "__Tick " << tick << "___ \n";
		cout << "Engine temperature = " << t << endl;

		if (!maxStage)
		{
			if (engine.V.size() == stage + 1)
			{
				cout << "Max stage\n";
				maxStage = true;
			}
			else
			{
				if (engine.V[stage + 1] < V)
				{
					stage++;

					a = engine.M[stage] / engine.I;

				}
			}
		}

		Vh = engine.M[stage] * engine.Hm + V * V * engine.Hv;

		Vc = engine.C * (tAmbient - t);

		t = t + Vh + Vc;

		V = V + a;

		if (t >= engine.tOverheat)
		{
			cout << "WARNING!  ENGINE OVERHEAT! \nTime: " << tick << "\nEnd temperature : " << t;
			break;
		}
		else if (tick > 1000)
		{
			cout << " I think this is too much \nLOGOUT! Sorry! \nLast temperature: " << t;

			break;
		}

		tick++;
	}
}