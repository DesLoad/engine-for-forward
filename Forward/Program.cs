﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace Forward
{
    public struct Engine
    {
        public int I;
        public List<int> M;
        public List<int> V;
        public int tOverheat;
        public float Hm;
        public float Hv;
        public float C;
    }

    class Program
    {
        static void Main(string[] args)
        {
            #region Initialize
            //Установка конфигураций
            //Set config 
            Engine engine = new Engine
            {
                I = 10,
                M = new List<int> { 20, 75, 100, 105, 75, 0 },
                V = new List<int> { 0, 75, 150, 200, 250, 300 },
                tOverheat = 110,
                Hm = 0.01f,
                Hv = 0.0001f,
                C = 0.1f
            };
            //Ambient temperature
            int tAmbient = 0;

            //logic data
            int tick = 0;       //Time count
            float t;            //Start tempearture
            int stage = 0;      //Engine controller
            float V = 0;        //Crankshaft speed
            float a;            //Crankshaft acceleration

            float Vh;           //Speed of heating
            float Vc;           //Speed of freezing
            #endregion


            //Checking for correct data entry
            bool isCorrect = false;
            Console.WriteLine("Enter ambient trmperature (int) \n" +
                    "ATTENTION! \nPlease don't enter small and negative values, the program execution time will increase. Thank you.");
            while (!isCorrect)
            {
                isCorrect = int.TryParse(Console.ReadLine(), out tAmbient);
                if (!isCorrect)
                {
                    Console.WriteLine("ERROR: Incorrect value \n Enter new value");
                }
            }

            //Coefficient...Sorry, i bad know this part in physics

            //Logic
            t = tAmbient;
            a = engine.M[stage] / engine.I;
            bool maxStage = false;
            Console.WriteLine("Engine on");
            while (true)
            {
                Console.WriteLine($"____________________tick_-_{tick}_____________________");
                Console.WriteLine($"Engine temperature = {t}");

                //Controle 
                if (!maxStage)
                {
                    if (engine.V.Count == stage + 1)
                    {
                        Console.WriteLine("Max Stage");
                        maxStage = true;
                    }
                    else
                    {
                        if (engine.V[stage + 1] < V)
                        {
                            stage++;

                            a = engine.M[stage] / engine.I;
                            //Place this command her becausein the code below it will take time to calculate the same number
                        }
                    }
                }
                Vh = engine.M[stage] * engine.Hm + V * V * engine.Hv;

                Vc = engine.C * (tAmbient - t);

                t = t + Vh + Vc;

                V = V + a;

                if (t >= engine.tOverheat)
                {
                    Console.WriteLine($"WARNING!  ENGINE OVERHEAT! \nTime: {tick}; \nEnd temperature: {t}");
                    break;
                }
                else if (tick > 1000)
                {
                    Console.WriteLine($"I think this is too much \nLOGOUT! Sorry! \nLast temperature: {t}");

                    break;
                }

                tick++;
            }
        }

    }
}
